build:
	cargo b -j8 --release --target wasm32-unknown-unknown
	-mv target/wasm32-unknown-unknown/release/*.wasm ./public/main.wasm
	notify-send "Done compiling!!!"

# doc:
# 	cargo doc -j8 -p mrcalculator
# 	cp -rf target/doc public/
# 	cargo readme > README.md

clean:
	cargo clean
	rm -rf public/main.wasm public/doc

init:
	basic-http-server ./public
