use macroquad::*;
pub async fn main(){

    let fejl_i_code = r"
        float tal2 = 3.5;
        int tal1 = 2;
        int tal3 = tal1 + tal2;
        
        println(tal3);

        // Error occurs because tal1 and tal2 do
        // not have same data type, and operators
        // in processing are only defined
        // for primitives of same data type.
    ";

    loop{
        clear_background(BLACK);

        for (n, line) in fejl_i_code.split("\n").enumerate(){
            draw_text(
                line,
                10.,
                28. * n as f32,
                30.,
                WHITE,
            );
        }

        esc_btn!();
        next_frame().await
    }
}
