use macroquad::*;

pub async fn main(){
    loop{
        clear_background(BLACK);

        for n in 0..5{
            let x = 10. + n as f32 * 20.; // Line x position.
            let h = 40. + n as f32 * 50.;// Line height. 

            draw_text(
                &format!("x = {}, h = {}", x, h),
                120.,
                n as f32 * 28.,
                30.,
                WHITE,
            );

            draw_line(x, 10., x, h, 5., WHITE); // The 5 is the thickness of the line
        }
         draw_text(
            "Idk hvorfor det ikke ser ud som i opgaven :/",
            50.,
            400.,
            30.,
            WHITE,
        );

        esc_btn!();
        next_frame().await
    }
}
