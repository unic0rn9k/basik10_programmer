use macroquad::*;
pub async fn main(){
    
    let r = ||rand::gen_range(0,100) as f32 / 100.; 
    let new_color = ||Color::new(r(), r(), r(), 1.);
    
    let mut color = new_color();

    let mut ball = (100., 53.);
    let mut dir = (9f32, 4f32);

    let game_size = screen_width().min(screen_height()); // Get smallest axis of screen

    loop{
        clear_background(BLACK);
         
        draw_text(
            "I think I over did this one...",
            100.,
            100.,
            30.,
            WHITE,
        );

        draw_circle(
            ball.0 - dir.0,
            ball.1 - dir.1,
            20.,
            new_color()
        );
        
        draw_circle(ball.0, ball.1, 20., color);

        let mut update_dir = |p: f32, d: &mut f32| {
            if p < 0.{
                color=new_color();
                *d = d.abs()
            }else if p > game_size{
                color=new_color();
                *d = d.abs() * -1f32
            }else{
                *d += 0.5 - r()
            }
        };

        update_dir(ball.0, &mut dir.0);
        update_dir(ball.1, &mut dir.1);

        ball.0+=dir.0;
        ball.1+=dir.1;

        esc_btn!();
        next_frame().await
    }
}
