use macroquad::*;

pub async fn main(){
    const TEXT: &str = r"
        Du sagde det var fint
        hvis vi ikke fik lavet alle opgaverne...
        This one i'll do next time then

        - Aksel
    ";

    loop{
        clear_background(BLACK);

        for (n, line) in TEXT.split("\n").enumerate(){
            draw_text(
                line,
                10.,
                30. + n as f32 * 30.,
                30.,
                WHITE,
            );
        }

        esc_btn!();
        next_frame().await
    }
}
