use macroquad::*;
pub async fn main(){

    // Declare variables
    // oranges is suffixed with u16 so the compiler can infer data types for all variables
    let oranges = 69_u16;
    let apples = 420;
    let fruits = oranges + apples; // Add variables together.

    info!("fruits = {}", fruits); // Print fruits to console

    loop{ // Print fruits to browser canvas
        clear_background(BLACK);

        draw_text(
            &format!("fruits = {}", fruits),
            10.,
            10.,
            30.,
            WHITE,
        );

        esc_btn!();
        next_frame().await
    }
}
