use macroquad::*;
pub async fn main(){

    const TEXT: &[&str] = &[r#"
        "Imperative programming is a programming paradigm 
         that uses statements
         to modify the state of a program." - Wiki

        This means programs consists of 
        a set of instructions for the computer to execute.

        This is different to purely functional programing,
        where you can't to mutate variables outside scope,
        you can only evaluate arguments to a function,
        and return a value.

        The main method of computation in
        imperative programming, is mutating variables.

        Some key traits of imperative languages are:
         - global variables,
         - mutable first variables,
    "#,
    r#"
        The syntax of a programing language,
        is much like with real languages,
        how the text looks.
        That is rules for where you put tabs,
        brackets, semicolons and alike.
        
        It is the rules for matching expressions
        to statements.

        Semantics on the other hand is the functionality.
        That is to say what an expression does,
        rather than what it looks like.
    "#,
    r#"
        Control flow is the flow of control in the program,
        that is, in which order statements get executed.
        
        Control flow is controlled by control structures.
        Some examples of control structures are:

        Branches control if a statement gets executed,
        (0 or 1 times). This can be fx ifs, switches, and alike.

        Loops control how many times a statement get executed.
        (0 or more times). This can fx be for loops.
    "#,
    r#"
        A variable is an aliased piece of memory.
        For example:

        age = 3;

        Here we assign some memory with the value 3,
        and alias it 'age'.
        So we can now refer to that data with the keyword age.

        Some times a variable doesn't get assigned any memory,
        that might be because it gets optimized away
        by the compiler,
        or because its representation is abstract and
        is evaluated at compile time.
    "#,
    r#"
        A lot of language do these things differently,
        but these are the most common data types, and their uses.
 

        An int, or integer, is a whole number.
        Typically with numerical values, like ints, 
        the language will distinguish between
        signed and unsigned values.

        a float is a floating point number.

        a char is a charecter, typically 8-bits (1-byte),
        but in some languages (like Go) they are 16-bit,
        to allow for unicode symbols, emojis, and alike.

        a string is a sequences of chars,
        or a reference to a byte array, depends on the language.

        a bool, or boolean, is a single bit.
        They are typically used to evaluate branch's.

    "#
    ];

    let mut page = 0;
    let mut was_mouse_pressed = true;

    loop{
        clear_background(BLACK);

        for (n, line) in TEXT[page].split("\n").enumerate(){
            draw_text(
                line,
                50.,
                50. + 25. * n as f32,
                25.,
                WHITE,
            );
        }

        draw_rectangle(400., 800., 150., 40., WHITE);
        draw_text("NEXT PAGE", 403., 800., 30., BLACK);

        let is_mouse_pressed = is_mouse_button_down(MouseButton::Left);
        if is_mouse_pressed && !was_mouse_pressed{
            was_mouse_pressed = true;
            page+=1;
            if page >= TEXT.len(){return;}
        }else{
            was_mouse_pressed = is_mouse_pressed;
        }

        esc_btn!();
        next_frame().await
    }
}
