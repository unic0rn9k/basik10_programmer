use macroquad::*;
pub async fn main(){
    loop{
        clear_background(BLACK);

        const SORRY: &str = r"
            Sorry, I didn't completely understand
            how we were supposed to solve
            the problem as described.
        ";

        for n in 0..11{
            let n = n as f32; // Convert n to a 32-bit float
            let c = Color::new(n/50., 0., (11. -n) / 11., 1.);
            // The colors in macroquad are represented with p values, that is brightness values
            // that goes from 0 to 1. This is not very memory efficient, but makes for some pretty
            // intuitive code.
            //
            // The red color is a 5th of full strength and increases to the right
            // Blue decreases to the right and is goes from 1 to 0

            draw_circle(40. + n * 40., 40., 20., c);
            draw_circle(40. + n * 40., 80., 20., c);
        }

        for (n, line) in SORRY.split("\n").enumerate(){
            draw_text(
                line,
                50.,
                100. + n as f32 * 28.,
                30.,
                WHITE,
            );
        }

        esc_btn!();
        next_frame().await
    }
}
