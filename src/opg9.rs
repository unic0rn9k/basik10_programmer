use macroquad::*;

fn sqrt(x: f32) -> f32{
    let sqrt = x.sqrt();
    info!("{}", sqrt);
    sqrt
}

fn is_quadratic(x: f32) -> bool{
    x.sqrt() % 1. == 0.
}

pub async fn main(){
    const NUMBERS: &[f32] = &[
        3.,
        2.,
        4.,
        16.,
        32.,
        64.,
        13.,
    ];

    loop{
        clear_background(BLACK);

        for (y, n) in NUMBERS.iter().enumerate(){
            draw_text(
                &format!(
                    "sqare root of {} is {} and it is {}quadratic",
                    n,
                    sqrt(*n),
                    if is_quadratic(*n){""}else{"not "},
                ),
                10.,
                30. * y as f32 + 10.,
                30.,
                WHITE,
            );
        }

        esc_btn!();
        next_frame().await
    }
}
