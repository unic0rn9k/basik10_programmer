
// Im not entirely sure if I did this as intended :/

use macroquad::*;

fn circles(len: usize, y: f32){
     for n in 0..len{
        let n = n as f32; // Convert n to a 32-bit float
        let c = Color::new(n/50., 0., (11. -n) / 11., 1.);

        draw_circle(40. + n * 40., y, 20., c);
    }
}

// move is alrewady a function in rust, so i cant use it.
async fn move_circles(len: usize, speed: f32) -> bool{
    let game_size = screen_width().min(screen_height()); // Get smallest axis of screen

    // Set starting point based on sign of direction.
    let mut y = if speed.signum() == 1.{1.}else{game_size as f32};

    while y <= game_size && y > 0.{
        clear_background(BLACK);
        circles(len, y);
        y += speed; // Move the y position of the circles one unit of speed

        if is_key_down(KeyCode::Space){return true}
        next_frame().await
    }                    
    false
}

pub async fn main(){
    let mut dir = false; // Bool used to alternate between moving up/down.
    loop{
        if move_circles(10, if dir{5.}else{-5.}).await{
            return;
        }
        dir = !dir;
    }
}
