use macroquad::*;

const ARENA_RADIUS: f32 = 400.;

// Function returns euclidean (but not actually, cause we don't take the square root of the
// distance, for better performance) distance between two 2d-vectors.
fn dist(a: (f32, f32), b: (f32, f32)) -> f32{
    (a.0 - b.0).powi(2)+
    (a.1 - b.1).powi(2)
}

#[derive(Clone, Copy, Debug)]
struct Ball{ // Data structure for a ball. Keeps track of position, direction and color.
    pos: (f32, f32),
    dir: (f32, f32),
    color: Color,
} 

impl Ball{
    fn update(&mut self){           // Implement an update function for the ball,
         self.pos.0 += self.dir.0;  // changes direction of the ball,
         self.pos.1 += self.dir.1;  // if its outside the arena.
                                    // And it also draws the ball to the screen.

        let dist_from_center = dist((ARENA_RADIUS, ARENA_RADIUS), self.pos);

        if dist_from_center > ARENA_RADIUS.powi(2){
            // If the ball is going outside the arena, calculate new trajectory
            let angle = rand::gen_range(0, 228) as f32 / 100.; // Random angle, within 2pi radial.
            let len = (self.dir.0.powi(2) + self.dir.1.powi(2)).sqrt(); // The speed of the ball.

            // Calculate x and y values for direction, given the angle.
            self.dir.0 = angle.cos().copysign(ARENA_RADIUS - self.pos.0) * len;
            self.dir.1 = angle.sin().copysign(ARENA_RADIUS - self.pos.1) * len;
        }

        draw_circle(self.pos.0, self.pos.1, 20., self.color);
    }
}

pub async fn main(){

    let mut hp = 20u8;
    let mut beast = Ball{
        dir: (10., 10.), // I just picked a random position and speed.
        pos: (51., 52.),
        color: RED,
    };

    let mut guy = Ball{
        dir: (-10., -10.),
        pos: (300., 300.),
        color: BLUE,
    };

    loop{
        clear_background(BLACK);
        draw_circle(ARENA_RADIUS, ARENA_RADIUS, ARENA_RADIUS, DARKGRAY); // Draw arena.


        beast.update();
        guy.update();

        if dist(beast.pos, guy.pos) < 20. * 20. * 2. && hp > 0{
            // Check if distance between balls is less than the sum of their radius's,
            // But only if beast is still alive.
            hp -= 1;
            if hp <= 0{ // Stop beast if dead and console log.
                beast.dir.0=0.;
                beast.dir.1=0.;
                info!("The beast is dead");
            }else{
                info!("The beast hp is now {}", hp);
            }
        }

        draw_text( // Draw beast hp, or "The beast is dead", depending on hp.
            &if hp > 0 {
                format!("BEAST HP: {}", hp)
            }else{
                "The beast is dead".to_string()
            },
            10.,
            10.,
            30.,
            WHITE
        );

        draw_text("Click or drag to move guy...", 10., 770., 30., WHITE);

        if is_mouse_button_down(MouseButton::Left){ // If mouse is pressed re-calculate trajectory
            let mouse_pos = mouse_position();
            draw_line(guy.pos.0, guy.pos.1, mouse_pos.0, mouse_pos.1, 3., BLUE);
            
            let mut delta = ( // Vector between guy and mouse.
                mouse_pos.0 - guy.pos.0,
                mouse_pos.1 - guy.pos.1,
            );

            let delta_len = (delta.0.powi(2) + delta.1.powi(2)).sqrt(); // Length of delta vector.
            delta.0 /= delta_len; // Normalize delta vector.
            delta.1 /= delta_len;

            let dir_len = (guy.dir.0.powi(2) + guy.dir.1.powi(2)).sqrt();
            // Length of direction vector.

            guy.dir.0 = delta.0 * dir_len; // Move guy towards mouse, but preserve previous speed.
            guy.dir.1 = delta.1 * dir_len; 
        }

        esc_btn!();
        next_frame().await
    }
}
