# 10 basik programmer

[![gitlab]](https://gitlab.com/unic0rn9k/mr-calculator)
[![try-it]](https://unic0rn9k.gitlab.io/basik10_programmer)

[gitlab]: https://img.shields.io/badge/gitlab-FC6D27?style=for-the-badge&labelColor=555555&logo=gitlab
[try-it]: https://img.shields.io/badge/try_it!-8da0cb?style=for-the-badge

Alle opgaver er besvaret i programmet.

Jeg burde nok også pointere at jeg har genbrugt kode fra lommeregneren til at lave hoved menuen.
Jeg tænkte det var fint, siden det ikke var en del af opgaven.
